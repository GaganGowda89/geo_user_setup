package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class index_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <title>Welcome to Geo World</title>\n");
      out.write("        <script language =\"javascript\">\n");
      out.write("            function validate()\n");
      out.write("            {\n");
      out.write("                var uname = document.LoginForm.txt_uname; //get the username\n");
      out.write("                var passwd = document.LoginForm.txt_passwd //get the passwd     \n");
      out.write("            \n");
      out.write("               if (uname.value == null || uname.value == \"\")\n");
      out.write("                {\n");
      out.write("                    window.alert(\"Please enter the Username\");\n");
      out.write("                    uname.style.background = \"#f08080\";\n");
      out.write("                    uname.focus();\n");
      out.write("                    return false;\n");
      out.write("                }\n");
      out.write("               if (passwd.value == null || passwd.value == \"\")\n");
      out.write("                {\n");
      out.write("                    window.alert(\"Please enter the Password\");\n");
      out.write("                    passwd.style.background = \"#f08080\";\n");
      out.write("                    passwd.focus();\n");
      out.write("                    return false;\n");
      out.write("                }\n");
      out.write("            }\n");
      out.write("        </script>    \n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("        <center>\n");
      out.write("        <h2>Login</h2>\n");
      out.write("        <form method=\"post\" action =\"LoginController\" name =\"LoginForm\" onsubmit =\"return validate();\">\n");
      out.write("        \n");
      out.write("    Username  : <input type = \"text\" name=\"txt_uname\">\n");
      out.write("    Password  : <input type = \"text\" name=\"txt_passwd\">\n");
      out.write("    \n");
      out.write("    <input type=\"submit\" name=\"btn_login\" value=\"Login\">\n");
      out.write("    <h3> Don't have a account? <a href=\"register.jsp\">Register Here !! </a></h3>\n");
      out.write("    \n");
      out.write("    \n");
      out.write("    </center>\n");
      out.write("    </body>\n");
      out.write("</html>\n");
      out.write("\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
