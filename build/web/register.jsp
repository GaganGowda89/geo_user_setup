<%-- 
    Document   : register
    Created on : Oct 31, 2018, 2:02:04 PM
    Author     : GaganMGowda
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Register to explore GeoWorld</title>
        <script language="javascript">
            function validate(){
                var first_name = /^[a-z A-Z]+$/; //pattern allowed a-z or A-Z
                var last_name  = /^[a-z A-Z]+$/;
                var user_name  = /^[a-z A-Z]+$/;
                var passwd_valid = /^[a-z A-Z 0-9]{6,12}+$/; //pattern password allowed A to Z, a to z, 0-9 and 6 to 12 range
             
                var fname = document.getElementByID("fname");
                var lname = document.getElementByID("lname");
                var usrname = document.getElementByID("uname");
                var passwd_valid = document.getElementByID("passwd");
                
                    if(!fname.test(fname.value) || fname.value="") //apply if condition using test() method match the parameter for pattern allow alphabet only
                    {
                        alert("Please enter just the Alphabets");
                        fname.focus();
                        fname.style.background = '#f08080';
                        return false;   
                    }
                    if(!lname.test(lname.value) || lname.value="")
                    {
                        alert("Please enter just the Alphabets");
                        lname.focus();
                        lname.style.background = '#f08080';
                        return false;
                    }
                    if(!usrname.test(usrname.value) || usrname.value="")
                    {
                        alert("Please enter just the Alphabets");
                        usrname.focus();
                        usrname.style.background = '#f08080';
                        return false;
                    }
                    if(!passwd_valid.test(passwd_valid.value) || passwd_valid.value="")
                    {
                        alert("Please choose ur passwords with numbers and alphabets and in Range 6-12");
                        passwd_valid.focus();
                        passwd_valid.style.background = '#f08080';
                    }
                                }
        </script>
    </head>
    <body>
      <center>
        <h1>Register to explore GeoWorld !!</h1>
        
        <form method ="post" action="RegistrationController" onsubmit="return validate();">
            
            FirstName : <input type="text" name ="txt_fname" id ="fname"></br></br>
            LastName : <input type="text" name ="txt_lname" id="lname"> </br></br>
            Username : <input type="text" name="txt_uname" id="uname"> </br></br>
            Password : <input type="password" name="txt_passwd" id="passwd"></br></br>
            
            <input type="submit" name="btn_register" value="Register">
            
            <h3> You have a account? Sign In <a href="index.jsp"> here </a> </h3>
        </form>
        
      </center> 
    </body>
</html>
