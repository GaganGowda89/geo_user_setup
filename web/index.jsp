<%-- 
    Document   : index
    Created on : Oct 30, 2018, 4:32:52 PM
    Author     : GaganMGowda
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Welcome to Geo World</title>
        <script language ="javascript">
            function validate()
            {
                var uname = document.LoginForm.txt_uname; //get the username
                var passwd = document.LoginForm.txt_passwd //get the passwd     
            
               if (uname.value == null || uname.value == "")
                {
                    window.alert("Please enter the Username");
                    uname.style.background = "#f08080";
                    uname.focus();
                    return false;
                }
               if (passwd.value == null || passwd.value == "")
                {
                    window.alert("Please enter the Password");
                    passwd.style.background = "#f08080";
                    passwd.focus();
                    return false;
                }
            }
        </script>    
    </head>
    <body>
        <center>
        <h2>Login</h2>
        <form method="post" action ="LoginController" name ="LoginForm" onsubmit ="return validate();">
        
            Username  : <input type = "text" name="txt_uname">
            Password  : <input type = "text" name="txt_passwd">
    
            <input type="submit" name="btn_login" value="Login">
            <h3> Don't have a account? <a href="register.jsp"> Register Here !!! </a></h3>
    
        </form>
        
    </center>
    </body>
</html>

